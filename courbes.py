#===========================================================================================
#=                                                                                        ==
#=    Programme pour tracer les courbes du TD 8 : ecoulements supersoniques               ==
#=                                                                                        ==
#=                                                                        par Xiasu YANG  ==
#=                                                                            09/04/2019  ==
#===========================================================================================

#-importer les bibliotheques necessaires---------------------------------------------------
import numpy as np                                                                         #
import matplotlib.pyplot as plt                                                            #
#------------------------------------------------------------------------------------------

#-definir les courbes a tracer-------------------------------------------------------------
def fct_01(M):                     # fct f(M), TD compressibilite                          #
    f = (1 + gamma/2*M**2)/(1 + (gamma-1)/2*M**2)**(gamma/(gamma-1))                       #
    return f                                                                               #
vfct_01   = np.vectorize(fct_01)   # vectorisation de la fct_01                            #
                                                                                           #
def fct_02(Tt, M):                 # fct T = f(Tt, M), TD ecoulement interne               #
    T = Tt/(1 + (gamma-1)/2*M**2)                                                          #
    return T                                                                               #
vfct_02  = np.vectorize(fct_02)    # vectorisation de la fct_02                            #
                                                                                           #
def fct_03(Tt, M):                 # fct u = f(Tt, M), TD ecoulement interne               #
    u = M*(gamma*Rg*fct_02(Tt, M))**0.5                                                    #
    return u                                                                               #
vfct_03  = np.vectorize(fct_03)    # vectorisation de la fct_03                            #
#------------------------------------------------------------------------------------------

#-definir les constantes-------------------------------------------------------------------
# constantes physiques:                                                                    #
gamma      = 1.4    # Rapport des capacites thermiques Cp/Cv                               #
Rg         = 287.04 # Constante de gaz specifique (J.kg^-1.K^-1)                           #
# parametres des courbes: (a vous de jouer !!!)                                            #
N_pnts     = 100    # Nombre de points a tracer, l'augmenter pour des courbes plus lisses  #
N_crbs     = 5      # Nombre de courbes TD ecoulement interne a tracer, modifiable         #
Tt_min     = 288.0  # Tt minimale, modifiable jusqu'a Tt_min --> 0 K                       #
Tt_pas     = 20.0   # Increment de Tt entre 2 courbes, modifiable                          #
M_max      = 10     # M maximal, modifiable jusqu'a M --> +infinie                         #
#------------------------------------------------------------------------------------------

#-definir les variables independantes------------------------------------------------------
M_tab     = np.arange(N_pnts) * (M_max/(N_pnts-1)) #  M  varie de 0 a M_max defini         #
#------------------------------------------------------------------------------------------

#-definir l'abscisse-----------------------------------------------------------------------
major_ticks = np.arange(0, M_max, M_max/20)                                                #
#------------------------------------------------------------------------------------------

#-TD compressibilite, tracer Pt(V)/Pt(M) = f(M)--------------------------------------------
fig1 = plt.figure(figsize=(12,9))                                                          #
ax1  = fig1.add_subplot(1, 1, 1)                                                           #
rapport  = vfct_01(M_tab)                                                                  #
plt.plot(M_tab, rapport)                                                                   #
plt.xlim(0.0, M_max)                                                                       #
plt.ylim(0.0, 1.0)                                                                         #
plt.xlabel('Nombre de Mach')                                                               #
plt.ylabel('Rapport f(M)')                                                                 #
plt.title('TD compressibilite: Rapport des pressions totales f(M)')                        #
ax1.set_xticks(major_ticks)                                                                #
ax1.grid()                                                                                 #
plt.savefig('courbe_1.png')                                                                #
#------------------------------------------------------------------------------------------

#-TD compressibilite, tracer erreur = 1 - f(M)---------------------------------------------
fig2 = plt.figure(figsize=(12,9))                                                          #
ax2  = fig2.add_subplot(1, 1, 1)                                                           #
rapport  = vfct_01(M_tab)                                                                  #
plt.plot(M_tab, 1.0 - rapport)                                                             #
plt.xlim(0.0, M_max)                                                                       #
plt.ylim(0.0, 1.0)                                                                         #
plt.xlabel('Nombre de Mach')                                                               #
plt.ylabel('Erreur 1 - f(M)')                                                              #
plt.title('TD compressibilite: Erreur entre les pressions totales 1 - f(M)')               #       
ax2.set_xticks(major_ticks)                                                                #
ax2.grid()                                                                                 #
plt.savefig('courbe_2.png')                                                                #
#------------------------------------------------------------------------------------------

#-TD ecoulement interne, tracer T = f(Tt, M)-----------------------------------------------
fig3 = plt.figure(figsize=(12,9))                                                          #
ax3  = fig3.add_subplot(1, 1, 1)                                                           #
for i in range (N_crbs):                                                                   #
    Tt = i*Tt_pas + Tt_min                                                                 #
    T  = vfct_02(Tt, M_tab)                                                                #
    plt.plot(M_tab, T, label='Tt = %.1f' %Tt)                                              #
plt.xlim(0.0, M_max)                                                                       #
plt.ylim(0.0, 400)                                                                         #
plt.xlabel('Nombre de Mach')                                                               #
plt.ylabel('Temperature statique (K)')                                                     #
plt.legend()                                                                               #
plt.title('TD ecoulement interne: T = f(Tt, M)')                                           #
ax3.set_xticks(major_ticks)                                                                #
ax3.grid()                                                                                 #
plt.savefig('courbe_3.png')                                                                #
#------------------------------------------------------------------------------------------

#-TD ecoulement interne, tracer u = f(Tt, M)-----------------------------------------------
fig4 = plt.figure(figsize=(12,9))                                                          #
ax4  = fig4.add_subplot(1, 1, 1)                                                           #
for i in range (N_crbs):                                                                   #
    Tt = i*Tt_pas + Tt_min                                                                 #
    u  = vfct_03(Tt, M_tab)                                                                #
    plt.plot(M_tab, u, label='Tt = %.1f' %Tt)                                              #
plt.xlim(0.0, M_max)                                                                       #
plt.ylim(0.0, 1000)                                                                        #
plt.xlabel('Nombre de Mach')                                                               #
plt.ylabel('Vitesse u (m/s)')                                                              #
plt.legend()                                                                               #
plt.title('TD ecoulement interne: u = f(Tt, M)')                                           #
ax4.set_xticks(major_ticks)                                                                #
ax4.grid()                                                                                 #
plt.savefig('courbe_4.png')                                                                #
#------------------------------------------------------------------------------------------
